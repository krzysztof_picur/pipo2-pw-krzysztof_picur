﻿namespace Biblioteka
{
    partial class dodaj_ksiazke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dodajk = new System.Windows.Forms.Button();
            this.naklad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ISBN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.oprawa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gatunek = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.podtytul = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tytul = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dodajk);
            this.groupBox1.Controls.Add(this.naklad);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ISBN);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.oprawa);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.gatunek);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.podtytul);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tytul);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 390);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodaj książkę";
            // 
            // dodajk
            // 
            this.dodajk.Location = new System.Drawing.Point(41, 337);
            this.dodajk.Name = "dodajk";
            this.dodajk.Size = new System.Drawing.Size(112, 23);
            this.dodajk.TabIndex = 7;
            this.dodajk.Text = "Dodaj książkę";
            this.dodajk.UseVisualStyleBackColor = true;
            this.dodajk.Click += new System.EventHandler(this.dodajk_Click);
            // 
            // naklad
            // 
            this.naklad.Location = new System.Drawing.Point(20, 296);
            this.naklad.Name = "naklad";
            this.naklad.Size = new System.Drawing.Size(143, 20);
            this.naklad.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "nakład";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // ISBN
            // 
            this.ISBN.Location = new System.Drawing.Point(20, 247);
            this.ISBN.Name = "ISBN";
            this.ISBN.Size = new System.Drawing.Size(143, 20);
            this.ISBN.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "ISBN";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // oprawa
            // 
            this.oprawa.Location = new System.Drawing.Point(20, 199);
            this.oprawa.Name = "oprawa";
            this.oprawa.Size = new System.Drawing.Size(143, 20);
            this.oprawa.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "oprawa";
            // 
            // gatunek
            // 
            this.gatunek.Location = new System.Drawing.Point(20, 151);
            this.gatunek.Name = "gatunek";
            this.gatunek.Size = new System.Drawing.Size(143, 20);
            this.gatunek.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "gatunek";
            // 
            // podtytul
            // 
            this.podtytul.Location = new System.Drawing.Point(20, 102);
            this.podtytul.Name = "podtytul";
            this.podtytul.Size = new System.Drawing.Size(143, 20);
            this.podtytul.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "podtytuł";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tytul
            // 
            this.tytul.Location = new System.Drawing.Point(20, 47);
            this.tytul.Name = "tytul";
            this.tytul.Size = new System.Drawing.Size(143, 20);
            this.tytul.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "tytuł";
            // 
            // dodaj_ksiazke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 403);
            this.Controls.Add(this.groupBox1);
            this.Name = "dodaj_ksiazke";
            this.Text = "Dodaj ksiazke";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tytul;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox oprawa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox gatunek;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox podtytul;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ISBN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox naklad;
        private System.Windows.Forms.Button dodajk;
    }
}