﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Biblioteka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            admin_login a = new admin_login();
            a.ShowDialog();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=biblioteka;";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            user_register_and_login f2 = new user_register_and_login();

            try
            {
                databaseConnection.Open();
                if (databaseConnection.State == ConnectionState.Open)
                {
                    MessageBox.Show("Baza danych połączona");

                }
                else
                {

                    MessageBox.Show("Wystąpił problem z połączeniem");
                    databaseConnection.Close();
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            databaseConnection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            user_register_and_login f2 = new user_register_and_login();
            f2.ShowDialog();
        }
    }
}
