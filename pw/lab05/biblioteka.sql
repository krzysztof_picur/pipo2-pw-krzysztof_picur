-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Kwi 2020, 02:45
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biblioteka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autor`
--

CREATE TABLE `autor` (
  `id_autora` int(11) NOT NULL,
  `imie` varchar(20) DEFAULT NULL,
  `nazwisko` varchar(20) NOT NULL,
  `drugie_imie` varchar(20) DEFAULT NULL,
  `pseudonim` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `noblista` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `autor`
--

INSERT INTO `autor` (`id_autora`, `imie`, `nazwisko`, `drugie_imie`, `pseudonim`, `email`, `noblista`) VALUES
(1, 'Andrzej', 'Sapkowski', '', 'sapek', 'sapek@o2.pl', b'1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane_admina`
--

CREATE TABLE `dane_admina` (
  `id` int(11) NOT NULL,
  `nazwa_admina` varchar(50) NOT NULL,
  `haslo_admina` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `dane_admina`
--

INSERT INTO `dane_admina` (`id`, `nazwa_admina`, `haslo_admina`) VALUES
(1, 'star', 'vertigo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `PESEL` char(11) NOT NULL,
  `miasto` varchar(20) NOT NULL,
  `telefon` int(11) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `nazwa_uzytkownika` varchar(50) NOT NULL,
  `haslo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`PESEL`, `miasto`, `telefon`, `email`, `nazwa_uzytkownika`, `haslo`) VALUES
('78472874873', 'Wroclaw', 784368204, 'janek@gmail.com', 'Jan Kowalski', '1234');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ksiazka`
--

CREATE TABLE `ksiazka` (
  `id_ksiazki` int(11) NOT NULL,
  `tytul` varchar(50) NOT NULL,
  `podtytul` varchar(50) DEFAULT NULL,
  `gatunek` varchar(20) DEFAULT NULL,
  `oprawa` varchar(15) DEFAULT NULL,
  `ISBN` char(13) NOT NULL,
  `naklad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ksiazka`
--

INSERT INTO `ksiazka` (`id_ksiazki`, `tytul`, `podtytul`, `gatunek`, `oprawa`, `ISBN`, `naklad`) VALUES
(1, 'Wiedzmin', 'Miecz Przeznaczenia', 'fantastyka', 'twarda', '2458729932356', 12);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wydawnictwo`
--

CREATE TABLE `wydawnictwo` (
  `id_wydawnictwa` int(11) NOT NULL,
  `nazwa` varchar(35) NOT NULL,
  `rok_wydania` int(11) DEFAULT NULL,
  `miejsce_wydania` varchar(20) DEFAULT NULL,
  `typ_publikacji` varchar(15) DEFAULT NULL,
  `liczba_stron_wydania` int(11) DEFAULT NULL,
  `id_ksiazki` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wydawnictwo`
--

INSERT INTO `wydawnictwo` (`id_wydawnictwa`, `nazwa`, `rok_wydania`, `miejsce_wydania`, `typ_publikacji`, `liczba_stron_wydania`, `id_ksiazki`) VALUES
(1, 'Lubimy Czytac', 2008, 'Warszawa', 'Ksiazka', 444, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenia`
--

CREATE TABLE `wypozyczenia` (
  `id_wypozyczenia` int(11) NOT NULL,
  `id_ksiazki` int(11) DEFAULT NULL,
  `klient` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wypozyczenia`
--

INSERT INTO `wypozyczenia` (`id_wypozyczenia`, `id_ksiazki`, `klient`) VALUES
(1, 1, 'Jan Kowalski');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id_autora`);

--
-- Indeksy dla tabeli `dane_admina`
--
ALTER TABLE `dane_admina`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nazwa_admina` (`nazwa_admina`);

--
-- Indeksy dla tabeli `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`nazwa_uzytkownika`),
  ADD UNIQUE KEY `PESEL` (`PESEL`);

--
-- Indeksy dla tabeli `ksiazka`
--
ALTER TABLE `ksiazka`
  ADD PRIMARY KEY (`id_ksiazki`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD UNIQUE KEY `ISBN_2` (`ISBN`);

--
-- Indeksy dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  ADD PRIMARY KEY (`id_wydawnictwa`),
  ADD KEY `id_ksiazki` (`id_ksiazki`);

--
-- Indeksy dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  ADD PRIMARY KEY (`id_wypozyczenia`),
  ADD KEY `id_ksiazki` (`id_ksiazki`),
  ADD KEY `klient` (`klient`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `autor`
--
ALTER TABLE `autor`
  MODIFY `id_autora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `dane_admina`
--
ALTER TABLE `dane_admina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ksiazka`
--
ALTER TABLE `ksiazka`
  MODIFY `id_ksiazki` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  MODIFY `id_wydawnictwa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  MODIFY `id_wypozyczenia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `wydawnictwo`
--
ALTER TABLE `wydawnictwo`
  ADD CONSTRAINT `Wydawnictwo_ibfk_1` FOREIGN KEY (`id_ksiazki`) REFERENCES `ksiazka` (`id_ksiazki`);

--
-- Ograniczenia dla tabeli `wypozyczenia`
--
ALTER TABLE `wypozyczenia`
  ADD CONSTRAINT `Wypozyczenia_ibfk_2` FOREIGN KEY (`id_ksiazki`) REFERENCES `ksiazka` (`id_ksiazki`),
  ADD CONSTRAINT `Wypozyczenia_ibfk_3` FOREIGN KEY (`klient`) REFERENCES `klient` (`nazwa_uzytkownika`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
