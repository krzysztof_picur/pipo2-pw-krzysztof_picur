﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kulki : MonoBehaviour
{
    //Tabica GameObject[] w ktorej zapisywane sa obiekty kulka
    public GameObject[] kulka;

    // Start is called before the first frame update
    void Start()
    {
        //Po trzech sekundach od uruchomienia gry generuje losowa kulke
        
        //Invoke("GenerujKulki", 3f);

        //Pierwsze wywolanie funkcji GenerujKulki nastąpi po 3 sekundach
        //od uruchomienia gry a następne po sekundzie
       
        InvokeRepeating("GenerujKulki", 3f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        //Nacisniecie Enter powoduje zatrzymanie losowania kulek
        //Nacisniecie Spacji powoduje wygenerownie losowej kulki
    
       if(Input.GetKeyDown(KeyCode.KeypadEnter)) 
        {
            GenerujKulki();
            CancelInvoke("GenerujKulki");
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            GenerujKulki();
        }

        if(Input.GetKey(KeyCode.Alpha1))
         {
             Instantiate(kulka[0], transform.position, Quaternion.identity);
        }

        if(Input.GetKey(KeyCode.Alpha2))
         {
             Instantiate(kulka[1], transform.position, Quaternion.identity);
        }

        if(Input.GetKey(KeyCode.Alpha3))
         {
             Instantiate(kulka[2], transform.position, Quaternion.identity);
        }

        if(Input.GetKey(KeyCode.Alpha4))
         {
             Instantiate(kulka[3], transform.position, Quaternion.identity);
        }

        if(Input.GetKey(KeyCode.Alpha5))
        {
             Instantiate(kulka[4], transform.position, Quaternion.identity);
        }

    }

    //Funkcja generujaca losowe kulki
    void GenerujKulki()
    {
      int los=Random.Range(0, kulka.Length);
      Instantiate(kulka[los], transform.position, Quaternion.identity);  
    }
}
