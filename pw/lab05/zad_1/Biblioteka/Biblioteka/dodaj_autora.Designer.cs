﻿namespace Biblioteka
{
    partial class dodaj_autora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.noblista = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pseudonim = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.drugie_imie = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nazwisko = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.imie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.noblista);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.pseudonim);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.drugie_imie);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nazwisko);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.imie);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 398);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodaj autora";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 341);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Dodaj autora";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // noblista
            // 
            this.noblista.AutoSize = true;
            this.noblista.Location = new System.Drawing.Point(19, 298);
            this.noblista.Name = "noblista";
            this.noblista.Size = new System.Drawing.Size(70, 17);
            this.noblista.TabIndex = 1;
            this.noblista.Text = "TAK/NIE";
            this.noblista.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 282);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "noblista";
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(19, 250);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(144, 20);
            this.email.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "email";
            // 
            // pseudonim
            // 
            this.pseudonim.Location = new System.Drawing.Point(19, 202);
            this.pseudonim.Name = "pseudonim";
            this.pseudonim.Size = new System.Drawing.Size(144, 20);
            this.pseudonim.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "pseudonim";
            // 
            // drugie_imie
            // 
            this.drugie_imie.Location = new System.Drawing.Point(19, 154);
            this.drugie_imie.Name = "drugie_imie";
            this.drugie_imie.Size = new System.Drawing.Size(144, 20);
            this.drugie_imie.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "drugie imię";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // nazwisko
            // 
            this.nazwisko.Location = new System.Drawing.Point(19, 101);
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(144, 20);
            this.nazwisko.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "nazwisko";
            // 
            // imie
            // 
            this.imie.Location = new System.Drawing.Point(19, 48);
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(144, 20);
            this.imie.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "imię";
            // 
            // dodaj_autora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 399);
            this.Controls.Add(this.groupBox1);
            this.Name = "dodaj_autora";
            this.Text = "Dodaj autora";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nazwisko;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox imie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox noblista;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox pseudonim;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox drugie_imie;
    }
}