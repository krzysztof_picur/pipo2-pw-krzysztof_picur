﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kulka : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public GameObject[] kuleczka;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    	//losowy kolor
    	int los=Random.Range(0, kuleczka.Length);
    	//plynny ruch obiektu
       float x = Input.GetAxis("Horizontal")*Time.deltaTime*speed;
       //print(x);
       float y = Input.GetAxis("Vertical")*Time.deltaTime*speed;
    	//print(y);

    	//Zmiana zależna od czasu a nie od frame
    	transform.Translate(x, y, 0);

    	//osbluga przyciskow

    	if(Input.GetMouseButtonDown(0))
    	{
    		//odczyt wspolrzednych kursora myszy po nacisnieciu LPM
    		Vector3 pozycja=Input.mousePosition;
    		pozycja.z = 15f;

    		//zamienia pozycje ekranu na pozycje swiata
    		pozycja=Camera.main.ScreenToWorldPoint(pozycja);

   			Instantiate(kuleczka[los], pozycja, Quaternion.identity);	
    	}

    	if(Input.GetMouseButtonDown(1))
    	{
    		print("PPM");
    	}

    	if(Input.GetMouseButtonDown(2))
    	{
    		print("SPM");
    	}

    }
}
