﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace lab4_BMI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if ((textBox1.Text != "") && (textBox2.Text != ""))
            {

                double wzrost = Convert.ToDouble(textBox1.Text);
                Debug.WriteLine(wzrost);
                double waga = Convert.ToDouble(textBox2.Text);
                Debug.WriteLine(waga);


                double BMI = waga / (wzrost * wzrost);

                wynik.Text = BMI.ToString();
                wynik.ForeColor = Color.Black;
            }
            else
            {
                wynik.ForeColor = Color.Red;
                wynik.Text = "Brak Danych!";
            }
        }

    }
}
