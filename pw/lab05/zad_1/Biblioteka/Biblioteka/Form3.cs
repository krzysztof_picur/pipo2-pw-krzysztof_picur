﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dadaj_wydawnictwo d= new dadaj_wydawnictwo();
            d.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dodaj_ksiazke d = new dodaj_ksiazke();
            d.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dodaj_autora d = new dodaj_autora();
            d.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form4 d = new Form4();
            d.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            wyswietl_ksiazki_a d = new wyswietl_ksiazki_a();
            d.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            wyswietl_autorow_a d = new wyswietl_autorow_a();
            d.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            wyswietl_wypozyczenias d = new wyswietl_wypozyczenias();
            d.ShowDialog();
        }
    }
}
