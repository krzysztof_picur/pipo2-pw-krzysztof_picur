﻿namespace Biblioteka
{
    partial class admin_login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_login));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.logowanie_admina = new System.Windows.Forms.Button();
            this.haslo_admina = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nazwa_admina = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightPink;
            this.groupBox1.Controls.Add(this.logowanie_admina);
            this.groupBox1.Controls.Add(this.haslo_admina);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nazwa_admina);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(323, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 228);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zaloguj się";
            // 
            // logowanie_admina
            // 
            this.logowanie_admina.Location = new System.Drawing.Point(61, 171);
            this.logowanie_admina.Name = "logowanie_admina";
            this.logowanie_admina.Size = new System.Drawing.Size(75, 23);
            this.logowanie_admina.TabIndex = 3;
            this.logowanie_admina.Text = "zaloguj się";
            this.logowanie_admina.UseVisualStyleBackColor = true;
            this.logowanie_admina.Click += new System.EventHandler(this.logowanie_admina_Click);
            // 
            // haslo_admina
            // 
            this.haslo_admina.Location = new System.Drawing.Point(12, 121);
            this.haslo_admina.Name = "haslo_admina";
            this.haslo_admina.PasswordChar = '*';
            this.haslo_admina.Size = new System.Drawing.Size(168, 20);
            this.haslo_admina.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "hasło";
            // 
            // nazwa_admina
            // 
            this.nazwa_admina.Location = new System.Drawing.Point(12, 60);
            this.nazwa_admina.Name = "nazwa_admina";
            this.nazwa_admina.Size = new System.Drawing.Size(168, 20);
            this.nazwa_admina.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "nazwa administratora";
            // 
            // admin_login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Name = "admin_login";
            this.Text = "Logowanie administratora";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button logowanie_admina;
        private System.Windows.Forms.TextBox haslo_admina;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nazwa_admina;
        private System.Windows.Forms.Label label1;
    }
}