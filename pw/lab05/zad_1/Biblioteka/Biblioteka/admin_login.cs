﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Biblioteka
{
    public partial class admin_login : Form
    {
        public admin_login()
        {
            InitializeComponent();
        }

        private void logowanie_admina_Click(object sender, EventArgs e)
        {
            string admin = nazwa_admina.Text;
            string pass = haslo_admina.Text;

            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=biblioteka;";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            MySqlDataAdapter sda = new MySqlDataAdapter("select count(*) from dane_admina where nazwa_admina='" + admin + "' and haslo_admina='" + pass + "'", databaseConnection);

            DataTable dt = new DataTable();
            sda.Fill(dt);

            if (dt.Rows[0][0].ToString() == "1")
            {

                MessageBox.Show("Zalogowno jako administrator");
                Form3 f = new Form3();
                this.Hide();
                f.ShowDialog();
            }
            else
            {
                MessageBox.Show("Błąd - błędnie wprowadzone dane");
            }
        }
    }
}
