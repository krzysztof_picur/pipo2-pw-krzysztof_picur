﻿namespace Biblioteka
{
    partial class dadaj_wydawnictwo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rok_wydania = new System.Windows.Forms.TextBox();
            this.dodaj_wydawnictwo = new System.Windows.Forms.Button();
            this.id_ksiazki = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.liczba_stron_wydania = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.typ_publikacji = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.miejsce_wydania = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nazwa = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rok_wydania);
            this.groupBox1.Controls.Add(this.dodaj_wydawnictwo);
            this.groupBox1.Controls.Add(this.id_ksiazki);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.liczba_stron_wydania);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.typ_publikacji);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.miejsce_wydania);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nazwa);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 372);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodaj wydawnictwp";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // rok_wydania
            // 
            this.rok_wydania.Location = new System.Drawing.Point(18, 102);
            this.rok_wydania.Name = "rok_wydania";
            this.rok_wydania.Size = new System.Drawing.Size(141, 20);
            this.rok_wydania.TabIndex = 7;
            // 
            // dodaj_wydawnictwo
            // 
            this.dodaj_wydawnictwo.Location = new System.Drawing.Point(37, 325);
            this.dodaj_wydawnictwo.Name = "dodaj_wydawnictwo";
            this.dodaj_wydawnictwo.Size = new System.Drawing.Size(122, 23);
            this.dodaj_wydawnictwo.TabIndex = 6;
            this.dodaj_wydawnictwo.Text = "Dodaj wydawnictwo";
            this.dodaj_wydawnictwo.UseVisualStyleBackColor = true;
            this.dodaj_wydawnictwo.Click += new System.EventHandler(this.dodaj_wydawnictwo_Click);
            // 
            // id_ksiazki
            // 
            this.id_ksiazki.Location = new System.Drawing.Point(21, 288);
            this.id_ksiazki.Name = "id_ksiazki";
            this.id_ksiazki.Size = new System.Drawing.Size(138, 20);
            this.id_ksiazki.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "id książki";
            // 
            // liczba_stron_wydania
            // 
            this.liczba_stron_wydania.Location = new System.Drawing.Point(20, 238);
            this.liczba_stron_wydania.Name = "liczba_stron_wydania";
            this.liczba_stron_wydania.Size = new System.Drawing.Size(139, 20);
            this.liczba_stron_wydania.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "liczba stron wydania";
            // 
            // typ_publikacji
            // 
            this.typ_publikacji.Location = new System.Drawing.Point(18, 190);
            this.typ_publikacji.Name = "typ_publikacji";
            this.typ_publikacji.Size = new System.Drawing.Size(141, 20);
            this.typ_publikacji.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "typ publikacji";
            // 
            // miejsce_wydania
            // 
            this.miejsce_wydania.Location = new System.Drawing.Point(18, 141);
            this.miejsce_wydania.Name = "miejsce_wydania";
            this.miejsce_wydania.Size = new System.Drawing.Size(141, 20);
            this.miejsce_wydania.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "miejsce wydania";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "rok wydania";
            // 
            // nazwa
            // 
            this.nazwa.Location = new System.Drawing.Point(18, 42);
            this.nazwa.Name = "nazwa";
            this.nazwa.Size = new System.Drawing.Size(141, 20);
            this.nazwa.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "nazwa";
            // 
            // dadaj_wydawnictwo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 391);
            this.Controls.Add(this.groupBox1);
            this.Name = "dadaj_wydawnictwo";
            this.Text = "Dodaj wydawnictwo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nazwa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox id_ksiazki;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox liczba_stron_wydania;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox typ_publikacji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox miejsce_wydania;
        private System.Windows.Forms.Button dodaj_wydawnictwo;
        private System.Windows.Forms.TextBox rok_wydania;
    }
}